from django.shortcuts import render, redirect
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskCreateForm

# Create your views here.
@login_required()
def create_task(request):
    if request.method == "POST":
        form = TaskCreateForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskCreateForm()
    context = {"form": form}
    return render(request, "tasks/create.html", context)


@login_required
def list_tasks(request):
    list = Task.objects.filter(assignee=request.user)
    context = {
        "list_tasks": list,
    }
    return render(request, "tasks/list.html", context)

from django.shortcuts import render, redirect, get_object_or_404
from projects.models import Project
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectCreateForm

# Create your views here.


@login_required
def list_projects(request):
    list = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": list,
    }
    return render(request, "projects/list.html", context)


@login_required
def project_detail(request, id):
    project = get_object_or_404(Project, id=id)
    tasks = Task.objects.all()
    context = {
        "project_detail": project,
        "tasks": tasks,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectCreateForm(request.POST)
        if form.is_valid():
            new_project = form.save(False)
            new_project.owner = request.user
            new_project.save()
            return redirect("list_projects")
    else:
        form = ProjectCreateForm()

    context = {
        "form": form,
    }

    return render(request, "projects/create.html", context)
